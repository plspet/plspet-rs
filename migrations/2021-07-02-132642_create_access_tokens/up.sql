-- Your SQL goes here
CREATE TABLE access_tokens (
    id BIGINT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    token VARCHAR NOT NULL
)