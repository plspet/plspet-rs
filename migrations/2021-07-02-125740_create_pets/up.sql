-- Your SQL goes here
CREATE TABLE pets (
    id BIGINT PRIMARY KEY,
    managed_by BIGINT NOT NULL,
    slug VARCHAR NOT NULL UNIQUE,
    name VARCHAR NOT NULL,
    pets_received BIGINT NOT NULL
)