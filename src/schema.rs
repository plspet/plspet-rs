table! {
    access_tokens (id) {
        id -> Int8,
        user_id -> Int8,
        token -> Varchar,
    }
}

table! {
    pets (id) {
        id -> Int8,
        managed_by -> Int8,
        slug -> Varchar,
        name -> Varchar,
        pets_received -> Int8,
    }
}

table! {
    users (id) {
        id -> Int8,
        username -> Varchar,
        password -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    access_tokens,
    pets,
    users,
);
