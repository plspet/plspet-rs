#[macro_use]
extern crate diesel;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

use actix_web::{App, HttpServer, web};
use std::env;

mod routes;
mod structs;
mod schema;
mod models;
mod utils;

type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();

    // Load environment variables
    let address: String = env::var("LISTEN_ADDRESS")
        .expect("Cannot find LISTEN_ADDRESS");

    let database_address: String = env::var("DATABASE_URL")
        .expect("Cannot find DATABASE_URL");

    let sentry_address: String = env::var("SENTRY_URL")
        .expect("Cannot find SENTRY_URL");

    let crate_version: String = option_env!("CARGO_PKG_VERSION")
        .expect("Not compiled with Cargo!").to_string();

    env::set_var("RUST_BACKTRACE", "1");

    // Set up Sentry
    let _guard = sentry::init((sentry_address, sentry::ClientOptions {
        release: Some(format!("plspet@{}", crate_version).into()),
        auto_session_tracking: true,
        session_mode: sentry::SessionMode::Request,
        ..Default::default()
    }));

    // Set up database connection pool
    println!("Connecting to database");

    let connmanager = ConnectionManager::<PgConnection>::new(database_address);
    let dbpool = r2d2::Pool::builder()
        .build(connmanager)
        .expect("Failed to connect to database");

    println!("Listening on {}", &address);

    HttpServer::new(move ||
        App::new()
            // Sentry
            .wrap(sentry_actix::Sentry::new())
            // Database
            .app_data(web::Data::new(dbpool.clone()))
            // Pets
            .service(web::scope("/pet")
                .service(routes::pet::make_pet::make_pet)
                .service(routes::pet::edit_pet::edit_pet)
                .service(routes::pet::delete_pet::delete_pet)
                .service(routes::pet::pet_pet::pet_pet)
                .service(routes::pet::get_pet::get_pet)
            )
            // Users
            .service(web::scope("/user")
                .service(routes::user::create_user::create_user)
                .service(routes::user::log_in::login)
                .service(routes::user::log_out::logout)
                .service(routes::user::log_out::logoutall)
                .service(routes::user::edit_user::edit_username)
                .service(routes::user::edit_user::edit_password)
                .service(routes::user::get_username::get_username)
                .service(routes::user::get_user_pets::get_pets)
                .service(routes::user::delete_user::delete_user)
                
            )
            // Admin
            .service(web::scope("/admin"))
            // Health
            .service(web::scope("/health")
                .service(routes::sentry_actix_test::healthy)
                .service(routes::sentry_actix_test::failing)
            )
        )
        .bind(address)?
        .run()
        .await
}