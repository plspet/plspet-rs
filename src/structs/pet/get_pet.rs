use serde::Serialize;

#[derive(Serialize)]
pub struct PetResponse {
    pub slug: String,
    pub name: String,
    pub pets_received: i64,
    pub managed_by: i64
}