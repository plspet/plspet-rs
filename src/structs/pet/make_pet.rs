use serde::{Serialize, Deserialize};
use crate::schema::pets;

#[derive(Deserialize)]
pub struct NewPetReq {
    pub slug: String,
    pub name: String
}

#[derive(Serialize, Insertable)]
#[table_name="pets"]
pub struct NewPet {
    pub id: i64,
    pub managed_by: i64,
    pub slug: String,
    pub name: String,
    pub pets_received: i64
}