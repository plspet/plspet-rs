use serde::Serialize;

#[derive(Serialize)]
pub struct NotFound {
    pub slug: String,
    pub error: String
}