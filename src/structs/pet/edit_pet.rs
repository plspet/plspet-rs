use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct EditPetReq {
    pub slug: String,
    pub name: String
}


#[derive(Serialize)]
pub struct Error {
    pub id: i64,
    pub error: String
}