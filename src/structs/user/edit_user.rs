use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct UsernameEdit {
    pub username: String,
}

#[derive(Deserialize)]
pub struct PasswordEdit {
    pub password: String
}

#[derive(Serialize)]
pub struct Error {
    pub id: String,
    pub error: String
}