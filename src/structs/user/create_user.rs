use serde::{Deserialize, Serialize};
use crate::schema::users;

#[derive(Deserialize)]
pub struct Info {
    pub username: String,
    pub password: String
}

#[derive(Serialize, Insertable)]
#[table_name="users"]
pub struct NewAccount {
    pub id: i64,
    pub username: String,
    pub password: String
}

#[derive(Serialize)]
pub struct Error {
    pub username: String,
    pub error: String
}