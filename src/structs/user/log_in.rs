use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct Info {
    pub username: String,
    pub password: String
}


#[derive(Serialize)]
pub struct Error {
    pub username: String,
    pub error: String
}