use serde::Serialize;
use crate::schema::access_tokens;

#[derive(Serialize, Insertable)]
#[table_name="access_tokens"]
pub struct NewToken {
    pub id: i64,
    pub user_id: i64,
    pub token: String
}
