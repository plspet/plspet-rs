use actix_web::{web, Responder, delete, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;
use crate::utils::jwt::{UserAuth, get_token};
use crate::structs::pet::edit_pet::Error;

#[delete("/delete/{id}")]
pub async fn delete_pet(req: HttpRequest, pool: web::Data<DbPool>,  path: web::Path<i64>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let pet_id = path.into_inner();

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Check if pet exists
    let results = pets.filter(id.eq(&pet_id))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if results.len() < 1 {
        return HttpResponse::NotFound()
            .json(Error{
                id: pet_id,
                error: "ID not found in database!".to_string() })
    }

    // Check if pet is managed by user
    let mut correct_user: bool = false;

    let results = pets.filter(id.eq(&pet_id))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    for result in results {
        if result.managed_by == user.sub {
            correct_user = true;
        }
    }

    if !correct_user {
        return HttpResponse::Forbidden()
            .json(Error{
                id: pet_id,
                error: "Wrong user!".to_string()
            });
    }

    // Delete pet
    diesel::delete(pets.filter(id.eq(pet_id)))
        .execute(&dbconn)
        .expect("Error deleting pet!");

    return HttpResponse::Ok()
        .body("Goodbye!");
}