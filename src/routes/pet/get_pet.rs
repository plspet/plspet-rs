use actix_web::{web, Responder, get, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;

use crate::structs::pet::{not_found::NotFound, get_pet::PetResponse};


#[get("/{slug}")]
pub async fn get_pet(pool: web::Data<DbPool>, path: web::Path<String>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let web_slug = path.into_inner();

    let results = pets.filter(slug.eq(&web_slug))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if results.len() < 1 {
        return HttpResponse::NotFound()
            .json(NotFound{
                slug: web_slug,
                error: "Slug not found in database!".to_string() })
    }

    let mut pet_name: String = "".to_string();
    let mut pet_pets_received: i64 = 0;
    let mut pet_managed_by: i64 = 0;

    for result in results {
        pet_name = result.name;
        pet_pets_received = result.pets_received;
        pet_managed_by = result.managed_by;
    }

    return HttpResponse::Ok()
        .json(PetResponse{
            slug: web_slug,
            name: pet_name,
            pets_received: pet_pets_received,
            managed_by: pet_managed_by
        });
}