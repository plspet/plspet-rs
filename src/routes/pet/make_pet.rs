use actix_web::{web, Responder, post, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;
use snowflake::SnowflakeIdGenerator;

use crate::utils::jwt::{get_token, UserAuth};
use crate::structs::pet::make_pet::{NewPet, NewPetReq};
use crate::structs::pet::not_found::NotFound;

#[post("/new")]
pub async fn make_pet(req: HttpRequest, pool: web::Data<DbPool>, info: web::Json<NewPetReq>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Check if input is missing
    if info.0.slug.is_empty() || info.0.name.is_empty() {
        return HttpResponse::BadRequest()
            .json(NotFound{
                slug: info.0.slug,
                error: "Slug or name empty!".to_string()
            });
    }

    // Check if slug already exists
    let check_slug_exists = pets.filter(slug.eq(&info.0.slug))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if check_slug_exists.len() > 0 {
        return HttpResponse::Conflict()
            .json(NotFound{
                slug: info.0.slug,
                error: "Slug already taken!".to_string()
            });
    }

    // Initialize snowflake generator
    let mut snowflake_gen = SnowflakeIdGenerator::new(1, 1);

    // Generate pet id
    let pet_id = snowflake_gen.real_time_generate();

    // Insert new pet
    let new_pet = NewPet{
        id: pet_id.clone(),
        managed_by: user.sub.clone(),
        slug: info.0.slug.clone(),
        name: info.0.name.clone(),
        pets_received: 0
    };

    diesel::insert_into(pets)
        .values(&new_pet)
        .execute(&dbconn)
        .expect("Error inserting new pet!");

    return HttpResponse::Ok()
        .body(pet_id.to_string());
}