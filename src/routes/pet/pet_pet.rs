use actix_web::{web, Responder, put, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;
use crate::structs::pet::not_found::NotFound;

#[put("/pet/{slug}")]
pub async fn pet_pet(pool: web::Data<DbPool>,  path: web::Path<String>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let pet_slug = path.into_inner();

    // Check if pet exists
    let results = pets.filter(slug.eq(&pet_slug))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if results.len() < 1 {
        return HttpResponse::NotFound()
            .json(NotFound{
                slug: pet_slug,
                error: "Slug not found in database!".to_string() })
    }

    // Pet pet
    let mut new_amount_of_pets: i64 = 0;

    let results = diesel::update(pets.filter(slug.eq(pet_slug)))
        .set(pets_received.eq(pets_received + 1))
        .get_results::<crate::models::Pet>(&dbconn)
        .expect("Error deleting pet!");

    for result in results {
        new_amount_of_pets = result.pets_received;
    }

    return HttpResponse::Ok()
        .body((new_amount_of_pets).to_string());
}