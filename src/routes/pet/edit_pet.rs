use actix_web::{web, Responder, patch, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;

use crate::utils::jwt::{get_token, UserAuth};
use crate::structs::pet::edit_pet::{Error, EditPetReq};

#[patch("/edit/{id}")]
pub async fn edit_pet(req: HttpRequest, pool: web::Data<DbPool>, info: web::Json<EditPetReq>,  path: web::Path<i64>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let pet_id = path.into_inner();

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Check if input is missing
    if info.0.slug.is_empty() || info.0.name.is_empty() {
        return HttpResponse::BadRequest()
            .json(Error{
                id: pet_id,
                error: "Slug or name empty!".to_string()
            });
    }

    // Check if pet exists
    let results = pets.filter(id.eq(&pet_id))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if results.len() < 1 {
        return HttpResponse::NotFound()
            .json(Error{
                id: pet_id,
                error: "ID not found in database!".to_string() })
    }

    // Check if pet is managed by user & get current slug
    let mut correct_user: bool = false;
    let mut current_slug: String = "".to_string();

    let results = pets.filter(id.eq(&pet_id))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    for result in results {
        if result.managed_by == user.sub {
            correct_user = true;
            current_slug = result.slug;
        }
    }

    if !correct_user {
        return HttpResponse::Forbidden()
            .json(Error{
                id: pet_id,
                error: "Wrong user!".to_string()
            });
    }

    // Check if slug already exists (skip if slug is the same)
    let check_slug_exists = pets.filter(slug.eq(&info.0.slug))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if check_slug_exists.len() > 0 && current_slug != info.0.slug {
        return HttpResponse::Conflict()
            .json(Error{
                id: pet_id,
                error: "Slug already taken!".to_string()
            });
    }

    // Update pet
    diesel::update(pets.filter(id.eq(pet_id)))
        .set((slug.eq(info.0.slug), name.eq(info.0.name)))
        .execute(&dbconn)
        .expect("Error updating pet!");

    return HttpResponse::Ok()
        .body("Pet updated!");
}