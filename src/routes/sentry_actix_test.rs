use std::io;
use actix_web::{get, Error, HttpRequest};

#[get("/health")]
pub async fn healthy(_req: HttpRequest) -> Result<String, Error> {
    Ok("OK".into())
}

#[get("/sentry_test")]
pub async fn failing(_req: HttpRequest) -> Result<String, Error> {
    Err(io::Error::new(io::ErrorKind::Other, "An error happens here").into())
}
