use actix_web::{web, Responder, patch, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;
use zxcvbn::{zxcvbn, Entropy, feedback::Suggestion};
use sodiumoxide::crypto::pwhash::argon2id13;
use snowflake::SnowflakeIdGenerator;

use crate::utils::jwt::{get_token, UserAuth};
use crate::structs::user::edit_user::{UsernameEdit, PasswordEdit, Error};
use crate::structs::user::jwt_actions::NewToken;

#[patch("/edit/user")]
pub async fn edit_username(req: HttpRequest, pool: web::Data<DbPool>, info: web::Json<UsernameEdit>) -> impl Responder {
    use crate::schema::users::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Check if input is missing
    if info.0.username.is_empty() {
        return HttpResponse::BadRequest()
            .json(Error{
                id: user.sub.to_string(),
                error: "Password empty!".to_string()
            });
    }

    // Check if username already exists
    let check_username_exists_results = users.filter(username.eq(&info.0.username))
        .load::<crate::models::User>(&dbconn)
        .expect("Error loading users!");

    if check_username_exists_results.len() > 0 {
        return HttpResponse::Conflict()
            .json(Error{
                id: user.sub.to_string(),
                error: "Username already taken!".to_string()
            });
    }


    diesel::update(users.filter(id.eq(user.sub)))
        .set(username.eq(info.0.username))
        .execute(&dbconn)
        .expect("Error updating username!");

    return HttpResponse::Ok()
        .body("Username updated");
}

#[patch("/edit/pass")]
pub async fn edit_password(req: HttpRequest, pool: web::Data<DbPool>, info: web::Json<PasswordEdit>) -> impl Responder {
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Check if input is missing
    if info.0.password.is_empty() {
        return HttpResponse::BadRequest()
            .json(Error{
                id: user.sub.to_string(),
                error: "Password empty!".to_string()
            });
    }

    // Validate password
    let password_security_estimate: Entropy = zxcvbn(&info.0.password, &[]).expect("Cannot get estimate for password!");
    let mut password_fail_reason: Vec<String> = vec![];

    if &password_security_estimate.score() < &2 {
        for suggestion in password_security_estimate.feedback().as_ref().expect("Cannot get password feedback!").suggestions().into_iter() {
            match suggestion {
                Suggestion::UseAFewWordsAvoidCommonPhrases =>
                    password_fail_reason.push("UseAFewWordsAvoidCommonPhrases".to_string()),
                Suggestion::NoNeedForSymbolsDigitsOrUppercaseLetters =>
                    password_fail_reason.push("NoNeedForSymbolsDigitsOrUppercaseLetters".to_string()),
                Suggestion::AddAnotherWordOrTwo =>
                    password_fail_reason.push("AddAnotherWordOrTwo".to_string()),
                Suggestion::CapitalizationDoesntHelpVeryMuch =>
                    password_fail_reason.push("CapitalizationDoesntHelpVeryMuch".to_string()),
                Suggestion::AllUppercaseIsAlmostAsEasyToGuessAsAllLowercase =>
                    password_fail_reason.push("AllUppercaseIsAlmostAsEasyToGuessAsAllLowercase".to_string()),
                Suggestion::ReversedWordsArentMuchHarderToGuess =>
                    password_fail_reason.push("ReversedWordsArentMuchHarderToGuess".to_string()),
                Suggestion::PredictableSubstitutionsDontHelpVeryMuch =>
                    password_fail_reason.push("PredictableSubstitutionsDontHelpVeryMuch".to_string()),
                Suggestion::UseALongerKeyboardPatternWithMoreTurns =>
                    password_fail_reason.push("UseALongerKeyboardPatternWithMoreTurns".to_string()),
                Suggestion::AvoidRepeatedWordsAndCharacters =>
                    password_fail_reason.push("AvoidRepeatedWordsAndCharacters".to_string()),
                Suggestion::AvoidSequences =>
                    password_fail_reason.push("AvoidSequences".to_string()),
                Suggestion::AvoidRecentYears =>
                    password_fail_reason.push("AvoidRecentYears".to_string()),
                Suggestion::AvoidYearsThatAreAssociatedWithYou =>
                    password_fail_reason.push("AvoidYearsThatAreAssociatedWithYou".to_string()),
                Suggestion::AvoidDatesAndYearsThatAreAssociatedWithYou =>
                    password_fail_reason.push("AvoidDatesAndYearsThatAreAssociatedWithYou".to_string()),
            }
        }
    }

    if !password_fail_reason.is_empty() {
        return HttpResponse::NotAcceptable()
            .json(Error{
                id: user.sub.to_string(),
                error: password_fail_reason.join(", ")
            });
    }

    // Hash and salt password
    sodiumoxide::init().expect("Failed to initialize sodiumoxide!");

    let hashed_password_bytes = argon2id13::pwhash(
        info.0.password.as_bytes(),
        argon2id13::OPSLIMIT_INTERACTIVE,
        argon2id13::MEMLIMIT_INTERACTIVE,
    ).expect("Failed to create hashed password!");

    let hashed_password: String = std::str::from_utf8(&hashed_password_bytes.0)
        .expect("Failed to convert from utf8 to str!")
        .to_string()
        .trim_end_matches('\u{0}').parse().expect("Failed to trim hashed password!");

    // Initialize snowflake generator
    let mut snowflake_gen = SnowflakeIdGenerator::new(1, 1);

    // Create access token
    let new_access_token = crate::utils::jwt::encode_jwt(user.sub.clone().to_string());

    let new_token = NewToken{
        id: snowflake_gen.real_time_generate(),
        user_id: user.sub.clone(),
        token: new_access_token.clone()
    };

    // Update password in database
    diesel::update(crate::schema::users::dsl::users.filter(crate::schema::users::dsl::id.eq(&user.sub)))
        .set(crate::schema::users::dsl::password.eq(hashed_password))
        .execute(&dbconn)
        .expect("Error updating password!");

    // Log out all sessions
    diesel::delete(crate::schema::access_tokens::dsl::access_tokens.filter(crate::schema::access_tokens::dsl::user_id.eq(user.sub)))
        .execute(&dbconn)
        .expect("Error revoking access tokens!");

    // Insert new token
    diesel::insert_into(crate::schema::access_tokens::dsl::access_tokens)
        .values(&new_token)
        .execute(&dbconn)
        .expect("Error inserting access token!");

    return HttpResponse::Ok()
        .body(new_token.token);
}