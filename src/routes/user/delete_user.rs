use actix_web::{web, Responder, delete, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;
use crate::utils::jwt::{UserAuth, get_token};

#[delete("/delete")]
pub async fn delete_user(req: HttpRequest, pool: web::Data<DbPool>) -> impl Responder {
    use crate::schema::users::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    // Log out all sessions
    diesel::delete(crate::schema::access_tokens::dsl::access_tokens.filter(crate::schema::access_tokens::dsl::user_id.eq(user.sub)))
        .execute(&dbconn)
        .expect("Error revoking access tokens!");

    // Delete all owned pets
    diesel::delete(crate::schema::pets::dsl::pets.filter(crate::schema::pets::dsl::managed_by.eq(user.sub)))
        .execute(&dbconn)
        .expect("Error deleting pets!");

    // Delete account
    diesel::delete(users.filter(id.eq(user.sub)))
        .execute(&dbconn)
        .expect("Error deleting user!");

    return HttpResponse::Ok()
        .body("Goodbye!");
}