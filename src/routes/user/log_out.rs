use actix_web::{web, Responder, post, HttpResponse, HttpRequest};
use diesel::prelude::*;
use crate::DbPool;

use crate::utils::jwt::{get_token, UserAuth};

#[post("/logout")]
pub async fn logout(req: HttpRequest, pool: web::Data<DbPool>) -> impl Responder {
    use crate::schema::access_tokens::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    diesel::delete(access_tokens.filter(token.eq(user.token)))
        .execute(&dbconn)
        .expect("Error revoking access token!");

    return HttpResponse::Ok()
        .body("Logged Out");
}

#[post("/logoutall")]
pub async fn logoutall(req: HttpRequest, pool: web::Data<DbPool>) -> impl Responder {
    use crate::schema::access_tokens::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Verify and get token
    let user: UserAuth = match get_token(&req, &dbconn) {
        Some(user) => {
            if user.sub == 0 {
                return HttpResponse::Unauthorized()
                    .body("Missing/Wrong Token");
            }
            user
        },
        _ => {
            return HttpResponse::Unauthorized()
                .body("Missing/Wrong Token");
        }
    };

    println!("{}", &user.sub);

    diesel::delete(access_tokens.filter(user_id.eq(user.sub)))
        .execute(&dbconn)
        .expect("Error revoking access tokens!");

    return HttpResponse::Ok()
        .body("Logged Out");
}