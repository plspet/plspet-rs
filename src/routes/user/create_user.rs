use actix_web::{web, Responder, post, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;
use zxcvbn::{zxcvbn, Entropy, feedback::Suggestion};
use sodiumoxide::crypto::pwhash::argon2id13;
use snowflake::SnowflakeIdGenerator;

use crate::structs::user::{create_user::{Info, Error, NewAccount}, jwt_actions::NewToken};
use crate::schema::access_tokens::dsl::access_tokens;

#[post("/create")]
pub async fn create_user(pool: web::Data<DbPool>, info: web::Json<Info>) -> impl Responder {
    use crate::schema::users::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Check if input is missing
    if info.0.username.is_empty() || info.0.password.is_empty() {
        return HttpResponse::BadRequest()
            .json(Error{
                username: info.0.username,
                error: "Username or password empty!".to_string()
            });
    }

    // Check if username already exists
    let check_username_exists_results = users.filter(username.eq(&info.0.username))
        .load::<crate::models::User>(&dbconn)
        .expect("Error loading users!");

    if check_username_exists_results.len() > 0 {
        return HttpResponse::Conflict()
            .json(Error{
                username: info.0.username,
                error: "Username already taken!".to_string()
            });
    }

    // Validate password
    let password_security_estimate: Entropy = zxcvbn(&info.0.password, &[]).expect("Cannot get estimate for password!");
    let mut password_fail_reason: Vec<String> = vec![];

    if &password_security_estimate.score() < &2 {
        for suggestion in password_security_estimate.feedback().as_ref().expect("Cannot get password feedback!").suggestions().into_iter() {
            match suggestion {
                Suggestion::UseAFewWordsAvoidCommonPhrases =>
                    password_fail_reason.push("UseAFewWordsAvoidCommonPhrases".to_string()),
                Suggestion::NoNeedForSymbolsDigitsOrUppercaseLetters =>
                    password_fail_reason.push("NoNeedForSymbolsDigitsOrUppercaseLetters".to_string()),
                Suggestion::AddAnotherWordOrTwo =>
                    password_fail_reason.push("AddAnotherWordOrTwo".to_string()),
                Suggestion::CapitalizationDoesntHelpVeryMuch =>
                    password_fail_reason.push("CapitalizationDoesntHelpVeryMuch".to_string()),
                Suggestion::AllUppercaseIsAlmostAsEasyToGuessAsAllLowercase =>
                    password_fail_reason.push("AllUppercaseIsAlmostAsEasyToGuessAsAllLowercase".to_string()),
                Suggestion::ReversedWordsArentMuchHarderToGuess =>
                    password_fail_reason.push("ReversedWordsArentMuchHarderToGuess".to_string()),
                Suggestion::PredictableSubstitutionsDontHelpVeryMuch =>
                    password_fail_reason.push("PredictableSubstitutionsDontHelpVeryMuch".to_string()),
                Suggestion::UseALongerKeyboardPatternWithMoreTurns =>
                    password_fail_reason.push("UseALongerKeyboardPatternWithMoreTurns".to_string()),
                Suggestion::AvoidRepeatedWordsAndCharacters =>
                    password_fail_reason.push("AvoidRepeatedWordsAndCharacters".to_string()),
                Suggestion::AvoidSequences =>
                    password_fail_reason.push("AvoidSequences".to_string()),
                Suggestion::AvoidRecentYears =>
                    password_fail_reason.push("AvoidRecentYears".to_string()),
                Suggestion::AvoidYearsThatAreAssociatedWithYou =>
                    password_fail_reason.push("AvoidYearsThatAreAssociatedWithYou".to_string()),
                Suggestion::AvoidDatesAndYearsThatAreAssociatedWithYou =>
                    password_fail_reason.push("AvoidDatesAndYearsThatAreAssociatedWithYou".to_string()),
            }
        }
    }

    if !password_fail_reason.is_empty() {
        return HttpResponse::NotAcceptable()
            .json(Error{
                username: info.0.username,
                error: password_fail_reason.join(", ")
            });
    }

    // Hash and salt password
    sodiumoxide::init().expect("Failed to initialize sodiumoxide!");

    let hashed_password_bytes = argon2id13::pwhash(
        info.0.password.as_bytes(),
        argon2id13::OPSLIMIT_INTERACTIVE,
        argon2id13::MEMLIMIT_INTERACTIVE,
    ).expect("Failed to create hashed password!");

    let hashed_password = std::str::from_utf8(&hashed_password_bytes.0)
        .expect("Failed to convert from utf8 to str!")
        .to_string();

    // Initialize snowflake generator
    let mut snowflake_gen = SnowflakeIdGenerator::new(1, 1);

    // Create account details
    let account_id = snowflake_gen.real_time_generate();

    // Create access token
    let new_access_token = crate::utils::jwt::encode_jwt(account_id.clone().to_string());

    // Insert account details
    let new_account = NewAccount{
        id: account_id.clone(),
        username: info.0.username.clone(),
        password: hashed_password.trim_end_matches('\u{0}').parse().expect("Failed to trim hashed password!")
    };

    let new_token = NewToken{
        id: snowflake_gen.real_time_generate(),
        user_id: account_id.clone(),
        token: new_access_token.clone()
    };

    diesel::insert_into(users)
        .values(&new_account)
        .execute(&dbconn)
        .expect("Error inserting new user!");

    diesel::insert_into(access_tokens)
        .values(&new_token)
        .execute(&dbconn)
        .expect("Error inserting access token!");

    // Return user access token
    return HttpResponse::Ok()
        .body(new_access_token);
}