use actix_web::{web, Responder, post, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;
use sodiumoxide::crypto::pwhash::argon2id13;
use snowflake::SnowflakeIdGenerator;

use crate::structs::user::{log_in::{Info, Error}, jwt_actions::NewToken};
use crate::schema::access_tokens::dsl::access_tokens;

#[post("/login")]
pub async fn login(pool: web::Data<DbPool>, info: web::Json<Info>) -> impl Responder {
    use crate::schema::users::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    // Check if input is missing
    if info.0.username.is_empty() || info.0.password.is_empty() {
        return HttpResponse::BadRequest()
            .json(Error{
                username: info.0.username,
                error: "Username or password empty!".to_string()
            });
    }

    let mut hashed_pass_from_db: String = "".to_string();
    let mut hash_padding = [0u8; 128];
    let mut user_id: i64 = 0;

    // Match username to database and get password
    let user_lookup_results = users.filter(username.eq(&info.0.username))
        .load::<crate::models::User>(&dbconn)
        .expect("Error loading user from db!");

    for result in user_lookup_results {
        hashed_pass_from_db = result.password;
        user_id = result.id;
    }

    if user_id == 0 {
        return HttpResponse::BadRequest()
            .json(Error{
                username: info.0.username,
                error: "Username or password is incorrect!".to_string()
            });
    }

    // Convert hash to bytes vec
    hashed_pass_from_db
        .as_bytes()
        .iter()
        .enumerate()
        .for_each(|(i, byte)|{
            hash_padding[i] = byte.clone();
        });

    // Check if password is correct
    let pass_check = match argon2id13::HashedPassword::from_slice(&hash_padding) {
        Some(hp) => argon2id13::pwhash_verify(&hp, info.0.password.as_bytes()),
        _ => false,
    };

    return if pass_check {
        // Password correct

        // Initialize snowflake generator
        let mut snowflake_gen = SnowflakeIdGenerator::new(1, 1);

        // Create access token
        let new_access_token = crate::utils::jwt::encode_jwt(user_id.clone().to_string());

        // Insert token
        let new_token = NewToken{
            id: snowflake_gen.real_time_generate(),
            user_id: user_id.clone(),
            token: new_access_token.clone()
        };

        diesel::insert_into(access_tokens)
            .values(&new_token)
            .execute(&dbconn)
            .expect("Error inserting access token!");

        // Return user access token
        HttpResponse::Ok()
            .body(new_access_token)
    } else {
        // Password incorrect
        HttpResponse::BadRequest()
            .json(Error{
                username: info.0.username,
                error: "Username or password is incorrect!".to_string()
            })
    }
}