use actix_web::{web, Responder, get, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;

#[get("/{id}")]
pub async fn get_username(pool: web::Data<DbPool>, path: web::Path<i64>) -> impl Responder {
    use crate::schema::users::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let request_id: i64 = path.into_inner();
    let mut requested_username: String = "".to_string();

    let results = users.filter(id.eq(request_id))
        .load::<crate::models::User>(&dbconn)
        .expect("Error loading username from DB!");

    for result in results {
        requested_username = result.username
    }

    if requested_username.is_empty() {
        return HttpResponse::NotFound()
            .body("ID not found");
    }

    return HttpResponse::Ok()
        .body(requested_username);
}