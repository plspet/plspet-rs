use actix_web::{web, Responder, get, HttpResponse};
use diesel::prelude::*;
use crate::DbPool;
use crate::structs::pet::edit_pet::Error;

#[get("/pets/{id}")]
pub async fn get_pets(pool: web::Data<DbPool>, path: web::Path<i64>) -> impl Responder {
    use crate::schema::pets::dsl::*;
    let dbconn = pool.get().expect("Couldn't get DB connection from pool!");

    let req_id = path.into_inner();

    let results = pets.filter(managed_by.eq(&req_id))
        .load::<crate::models::Pet>(&dbconn)
        .expect("Error loading pets!");

    if results.len() < 1 {
        return HttpResponse::NotFound()
            .json(Error{
                id: req_id,
                error: "No pets found!".to_string() })
    }

    let mut pets_managed: Vec<i64> = vec![];

    for result in results {
        pets_managed.push(result.id)
    }

    return HttpResponse::Ok()
        .body(format!("{:?}", pets_managed));
}