#[derive(Queryable)]
pub struct Pet {
    pub id: i64,
    pub managed_by: i64,
    pub slug: String,
    pub name: String,
    pub pets_received: i64
}

#[derive(Queryable)]
pub struct User {
    pub id: i64,
    pub username: String,
    pub password: String,
}

#[derive(Queryable)]
pub struct AccessTokens {
    pub id: i64,
    pub user_id: i64,
    pub token: String
}