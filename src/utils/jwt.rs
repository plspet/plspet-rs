use serde::{Serialize, Deserialize};
use jsonwebtoken::{encode, decode, Header, Validation, EncodingKey, DecodingKey};
use std::time::{SystemTime, UNIX_EPOCH};
use jsonwebtoken::errors::{Error};
use actix_web::HttpRequest;
use r2d2::PooledConnection;
use diesel::r2d2::ConnectionManager;
use diesel::{PgConnection, QueryDsl};
use diesel::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    iss: String,
    exp: usize,
}

pub struct UserAuth {
    pub sub: i64,
    pub token: String
}

/// Encode a JWT with the plspet ISS and private key
pub fn encode_jwt(id: String) -> String {
    let private_key = include_bytes!("keys/private.pem").to_vec();

    let my_claims = Claims{
        sub: id,
        iss: "plspet".to_string(),
        exp: (SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() + 2592000000) as usize
    };

    let token = match encode(&Header::default(), &my_claims, &EncodingKey::from_secret(&*private_key)) {
        Ok(t) => t,
        Err(_) => panic!()
    };

    return token;
}

/// Verify the provided JWT was made with the provided private key and isn't expired
pub fn verify_jwt(jwt: &String) -> Result<Claims, Error> {
    let private_key = include_bytes!("keys/private.pem").to_vec();

    let validation = Validation{
        leeway: 240,
        validate_exp: true,
        ..Validation::default()
    };

    let token_data = decode::<Claims>(&jwt, &DecodingKey::from_secret(&*private_key), &validation)?;

    Ok(token_data.claims)
}

/// Get a user from access token
pub fn get_token(req: &HttpRequest, db: &PooledConnection<ConnectionManager<PgConnection>>) -> Option<UserAuth> {
    use crate::schema::access_tokens::dsl::*;

    let auth_header = req.headers()
        .get("Authorization")?
        .to_str()
        .ok()?;

    let auth_token: String = auth_header.replace("Bearer ", "");

    let key_vert = verify_jwt(&auth_token).unwrap();

    let mut user_auth = UserAuth{
        sub: 0,
        token: auth_token
    };

    let results = access_tokens.filter(user_id.eq(key_vert.sub.parse::<i64>().ok()?))
        .load::<crate::models::AccessTokens>(db).ok()?;

    for result in results {
        if key_vert.sub.parse::<i64>().unwrap() == result.user_id {
            user_auth.sub = result.user_id
        }
    }

    return Option::from(user_auth);
}